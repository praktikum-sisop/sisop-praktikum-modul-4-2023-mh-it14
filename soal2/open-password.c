#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <unistd.h>

// https://stackoverflow.com/questions/47093199/openssl-aes-decryption-in-c
// https://www.openssl.org/docs/manmaster/man3/BIO_f_base64.html

// install openssl
// sudo apt-get update
// sudo apt-get install libssl-dev

// compile
// gcc -o open-password open-password.c -lcrypto

char *base64_decode(const char *input, int length) {
    BIO *bio, *b64;
    char *buffer = (char *)malloc(length);
    memset(buffer, 0, length);

    bio = BIO_new_mem_buf((void *)input, length);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    BIO_read(bio, buffer, length);
    BIO_free_all(bio);

    return buffer;
}

int main() {
    // Buka file zip-pass.txt
    FILE *file = fopen("zip-pass.txt", "r");

    if (file == NULL) {
        perror("Error opening zip-pass.txt");
        return 1;
    }

    // Baca isi file zip-pass.txt
    char password_base64[100]; 
    fgets(password_base64, sizeof(password_base64), file);
    fclose(file);

    // Dekripsi password yang dienkripsi dalam Base64
    char *password = base64_decode(password_base64, strlen(password_base64));
    printf("Decrypted Password: %s\n", password);

    // Gunakan password untuk melakukan unzip pada home.zip
    char unzip_command[100];
    snprintf(unzip_command, sizeof(unzip_command), "unzip -P %s home.zip", password);
    int status = system(unzip_command);
    if (status == 0) {
        printf("Unzip successful!\n");
    }
    else {
        printf("Unzip failed. Please check the password.\n");
    }

    return 0;
}
