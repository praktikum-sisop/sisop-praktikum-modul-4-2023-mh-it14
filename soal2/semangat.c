#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

static const char *dirpath = "/home/erazora/Sisop-Modul-4/Soal-2";

// Fungsi untuk mencatat log ke dalam logs-fuse.log
void log_to_file(const char *tag, const char *information, int success) {
    FILE *log_file = fopen("logs-fuse.log", "a");
    if (log_file == NULL) {
        perror("Error opening logs-fuse.log");
        exit(EXIT_FAILURE);
    }

    time_t current_time;
    struct tm *tm_info;

    time(&current_time);
    tm_info = localtime(&current_time);

    char timestamp[20];
    strftime(timestamp, 20, "%d/%m/%Y-%H:%M:%S", tm_info);

    char log_message[1000];
    if (success) {
        snprintf(log_message, sizeof(log_message), "[SUCCESS]::%s::%s::%s\n", timestamp, tag, information);
    } else {
        snprintf(log_message, sizeof(log_message), "[FAILED]::%s::%s::%s\n", timestamp, tag, information);
    }

    fputs(log_message, log_file);
    fclose(log_file);
}

int password_bin(const char *inputpass){
    FILE *password_bin = fopen("../password.bin", "r");

    char Password[2000];
    if(fscanf(password_bin, "%s", Password) != 1){
        fclose(password_bin);
        perror("Error password");
        return 0;
    }

    fclose(password_bin);

    char cutpassword[9]; 
    strncpy(cutpassword, Password, 8);
    cutpassword[8] = '\0';

    memset(Password, 0, sizeof(Password));
    return strcmp(inputpass, cutpassword) == 0;
}
   
static int xmp_getattr(const char *path, struct stat *stbuf){ 
    char newdirpath[1000];
    char log_info[1000];

    if(strcmp(path,"/") == 0){
        path=dirpath;
        sprintf(newdirpath,"%s",path);
    }else sprintf(newdirpath,"%s%s",dirpath,path);

    sprintf(log_info, "Get Atribute %s", path);

    int res = lstat(newdirpath, stbuf);
    if(res == -1){
        log_to_file("getattr", log_info, 0);
        return -errno;
    }

    log_to_file("getattr", log_info, 1);
    return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value, size_t size) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lgetxattr(fpath, name, value, size);

    if (res == -1) {
        log_to_file("getxattr", path, 0);
        return -errno;
    }

    log_to_file("getxattr", path, 1);
    return res;
}

static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);

    if (res == -1) {
        log_event("setxattr", path, 0);
        return -errno;
    }

    log_event("setxattr", path, 1);
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ 
    //[2]
    char newdirpath[1000];
    char log_info[1000];
    
    if(strcmp(path,"/") == 0){
        path=dirpath;
        sprintf(newdirpath,"%s",path);
    }else sprintf(newdirpath, "%s%s",dirpath,path);

    sprintf(log_info, "Read Directory %s", path); 

    int res = 0;
    DIR *direktori;
    struct dirent *de;
    (void) offset;
    (void) fi;
    direktori = opendir(newdirpath);

    if(direktori == NULL) return -errno;

    while((de = readdir(direktori)) != NULL){
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));
        if(res!=0) break;
    }
    
    if(res == -1){
        log_to_file("readdir", log_info, 0);
        return -errno;
    }

    closedir(direktori);
    log_to_file("readdir", log_info, 1);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char newdirpath[1000];
    char log_info[1000];

    if(strcmp(path,"/") == 0){
        path = dirpath;
        sprintf(newdirpath,"%s",path);
    }else sprintf(newdirpath, "%s%s", dirpath, path);
 
    sprintf(log_info, "Read %s", path);

    int res = 0;
    int fd = 0 ;
    (void) fi;

    if(strstr(path, "/text") != NULL){
        char inputpw[1000];
        printf("password: ");
        scanf("%s", inputpw);

        if(!password_bin(inputpw)){
            printf("wrong password\n");
            return -EACCES;
        }
    }

    if(strstr(path, "/website/") != NULL && strcmp(path, "/website/content.csv") == 0){
        FILE *csv_file = fopen(path, "r");

        char line[1000];
        fgets(line, sizeof(line), csv_file); 

        while(fgets(line, sizeof(line), csv_file) != NULL){
            char file[200];
            char title[200];
            char body[300];
            sscanf(line, "%[^,],%[^,],%[^\n]", file, title, body);

            char html[1000];
            snprintf(html, sizeof(html), "%s/%s", dirpath, file);
            
            FILE *fileHTML = fopen(html, "w");
            fprintf(fileHTML, "<!DOCTYPE html>\n");
            fprintf(fileHTML, "<html lang=\"en\">\n");
            fprintf(fileHTML, "<head>\n");
            fprintf(fileHTML, "<meta charset=\"UTF-8\" />\n");
            fprintf(fileHTML, "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n");
            fprintf(fileHTML, "<title>%s</title>\n", title);
            fprintf(fileHTML, "</head>\n");
            fprintf(fileHTML, "<body>\n%s\n</body>\n", body);
            fprintf(fileHTML, "</html>\n");

            fclose(fileHTML);
        }
        fclose(csv_file);
        log_to_file("read", log_info, 1);
        return res;
    }

    fd = open(newdirpath, O_RDONLY);
    if(fd == -1) return -errno;
    res = pread(fd, buf, size, offset);

    if(res == -1){
        log_to_file("read", log_info, 0);
        return -errno;
    }

    close(fd);
    log_to_file("read", log_info, 1);
    return res;
}

static int xmp_mkdir(const char *path, mode_t mode){
    char newdirpath[1000];
    char log_info[1000];

    if(strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(newdirpath, "%s", path);
    }else{
        sprintf(newdirpath, "%s%s", dirpath, path);
    }

    sprintf(log_info, "Make Directory %s", path);

    int res = mkdir(newdirpath, mode);

    if(res == -1) {
        log_to_file("mkdir", log_info, 0);
        return -errno;
    }

    if(strcmp(path, "/documents") == 0){ //kategorikan doc folder
        DIR *direktori = opendir(dirpath);                                                      
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){                                               
            if(strstr(de->d_name, ".pdf") || strstr(de->d_name, ".docx")){                      
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);                                    
                //membuat path sebelum pemindahan dan sesudah pemindahan
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name); 
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){                                
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);                                                           
            }
        }
        closedir(direktori);
    } 

    else if(strcmp(path, "/images") == 0){ //kategorikan img
        DIR *direktori = opendir(dirpath);
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){
            if (strstr(de->d_name, ".jpg") || strstr(de->d_name, ".png") || strstr(de->d_name, ".ico")){
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name);
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);
            }
        }
        closedir(direktori);
    }

    else if(strcmp(path, "/website") == 0){ //kategorikan website file .js .html. json
        DIR *direktori = opendir(dirpath);
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){
            if(strstr(de->d_name, ".js") || strstr(de->d_name, ".html") || strstr(de->d_name, ".json")){
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name);
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);
            }
        }
        closedir(direktori);
    }

    else if(strcmp(path, "/sisop") == 0){
        DIR *direktori = opendir(dirpath);
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){
            if((strstr(de->d_name, ".c") || strstr(de->d_name, ".sh")) && !strstr(de->d_name, ".csv")){ //jika cuma .c maka .csv akan ikut
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name);
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);
            }
        }
        closedir(direktori);
    }

    else if(strcmp(path, "/text") == 0){ //kategorikan text file
        DIR *direktori = opendir(dirpath);
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){
            if(strstr(de->d_name, ".txt")){
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name);
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);
            }
        }
        closedir(direktori);
    }

    else if(strcmp(path, "/aI") == 0){ //kategorikan ai file
        DIR *direktori = opendir(dirpath);
        struct dirent *de;

        while((de = readdir(direktori)) != NULL){
            if(strstr(de->d_name, ".ipynb") || strstr(de->d_name, ".csv")){
                char pathsebelumnya[1000];
                char *pathsesudahnya = (char *)malloc(2000);
                snprintf(pathsebelumnya, sizeof(pathsebelumnya), "%s/%s", dirpath, de->d_name);
                snprintf(pathsesudahnya, 2000, "%s/%s", newdirpath, de->d_name);
                if(rename(pathsebelumnya, pathsesudahnya) != 0){
                    log_to_file("mkdir", log_info, 0);
                    closedir(direktori);
                    free(pathsesudahnya);
                    return -errno;
                }
                free(pathsesudahnya);
            }
        }
        closedir(direktori);
    }

    log_to_file("mkdir", log_info, 1);
    return 0;
}

static int xmp_rmdir(const char *path) { 
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    const char *dirname = strrchr(path, '/') + 1;     

    if (strncmp(dirname, "restricted", 10) == 0) {
        log_to_file("rmdir", path, 0);
        return -EACCES; 
    }

    int res = rmdir(fpath);
    if (res == -1) {
        log_to_file("rmdir", path, 0);
        return -errno;
    }

    log_to_file("rmdir", path, 1);
    return 0;
}

static int xmp_unlink(const char *path) { 
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    const char *filename = strrchr(path, '/') + 1; 

    if (strncmp(filename, "restricted", 10) == 0) {  
        log_to_file("unlink", path, "Failed to remove file - Restricted", 0);
        return -EACCES; 
    }

    int res = unlink(fpath);
    if (res == -1) {
        log_to_file("unlink", path, 0);
        return -errno;
    }

    log_to_file("unlink", path, 1);
    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) { 
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags | O_CREAT, mode);
    if (res == -1) {
        log_to_file("create", path, 0);
        return -errno;
    }

    fi->fh = res;

    log_to_file("create", path, 1);
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { 
    (void) path;

    int res = pwrite(fi->fh, buf, size, offset); 
    if (res == -1) {
        log_to_file("write", path, 0);
        return -errno;
    }

    char fpath[1000]; 
    sprintf(fpath, "%s%s", dirpath, path);

    log_to_file("write", path, 1);
    return res;
}

static int xmp_release(const char *path, struct fuse_file_info *fi) { 
    (void) path;
    int res = close(fi->fh);
    if (res == -1) {
        log_to_file("release", path, 0);
        return -errno;
    }

    log_to_file("release", path, 1);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) { 
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags); 
    if (res == -1) {
        log_event("open", path, 0);
        return -errno;
    }

    fi->fh = res;

    log_event("open", path, 1);
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .open = xmp_open,
    .release = xmp_release,
    .write = xmp_write,
    .create = xmp_create,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
};

int  main(int  argc, char *argv[]){ 
    umask(0);
    
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
