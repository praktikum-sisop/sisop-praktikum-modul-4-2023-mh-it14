#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define PORT 8080

//mencari all
void tampilfilm(char *respon){
    FILE *file = fopen("./aI/webtoon.csv", "r");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }

    char isi[200];
    int nomor = 1;
    while(fgets(isi, sizeof(isi), file) != NULL){
        char linenomor[300];
        sprintf(linenomor, "%d. %s", nomor, isi);
        strcat(respon, linenomor);
        nomor++;
    }
    fclose(file);
}

//mencari sesuai genre
void showgenre(char *genre, char *respon){
    FILE *file = fopen("./aI/webtoon.csv", "r");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }

    char isi[200];
    int nomor = 1;
    while(fgets(isi, sizeof(isi), file) != NULL){
        char judul[200];
        char genreInFile[200];
        char linenomor[300];
        sscanf(isi, "%*[^,],%[^,],%[^\n]", genreInFile, judul);

        if(strstr(genreInFile, genre) != NULL){
            sprintf(linenomor, "%d. %s\n", nomor, judul);
            strcat(respon, linenomor); 
            nomor++;
        }
    }
    fclose(file);
}

//mencari sesuai hari
void showhari(char *hari, char *respon){
    FILE *file = fopen("./aI/webtoon.csv", "r");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }

    char isi[200];
    int nomor = 1;
    while(fgets(isi, sizeof(isi), file) != NULL){
        char judul[200];
        char hariInFile[200];
        char linenomor[300];
        sscanf(isi, "%[^,],%*[^,],%[^\n]", hariInFile, judul);

        if(strstr(hariInFile, hari) != NULL){
            sprintf(linenomor, "%d. %s\n", nomor, judul);
            strcat(respon, linenomor); 
            nomor++;
        }
    }
    fclose(file);
}

//menambah film (hari,genre,judul)
void adding(char *film, char *respon){
    FILE *file = fopen("./aI/webtoon.csv", "a");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }

    fprintf(file, "%s\n", film);
    fclose(file);

    strcpy(respon, "Film berhasil ditambahkan.\n");
}

//menghapus film
void deleteing(char *judul, char *respon){
    FILE *file = fopen("./aI/webtoon.csv", "r");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }

    FILE *fileTemp = fopen("./aI/temp.csv", "w");
    if(fileTemp == NULL){
        strcpy(respon, "kesalahan buat temp.csv\n");
        return;
    }

    char isi[200];
    int deleted = 0;
    while(fgets(isi, sizeof(isi), file) != NULL){
        char judulInFile[200];
        sscanf(isi, "%*[^,],%*[^,],%[^\n]", judulInFile);

        if(strstr(judulInFile, judul) == 0){
            fprintf(fileTemp, "%s", isi);
        }else{
            deleted = 1;
        }
    }


    fclose(file);
    fclose(fileTemp);

    remove("./aI/webtoon.csv");
    rename("./aI/temp.csv", "./aI/webtoon.csv");

    if(deleted){
        strcpy(respon, "Film berhasil dihapus\n");
    }else{
        strcpy(respon, "samting wong\n");
    }
    
}
    
int main(int argc, char const *argv[]) {

    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[2048] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    while(1){
        char message[200];
        valread = read(new_socket, buffer, 2048);

        if(valread <= 0) break;

        sscanf(buffer, "%s", message);
        
        char respon[200];
        if(strncmp(message, "tampilkan", 9) == 0){
            tampilfilm(respon);
            printf("Received: %s\n\n", message);
        }else if(strncmp(message, "genre", 5) == 0){
            char genre[100];
            sscanf(buffer + 6, "%s", genre);
            showgenre(genre, respon);
            printf("Received: %s %s\n\n", message, genre);
        }else if(strncmp(message, "hari", 4) == 0){
            char hari[100];
            sscanf(buffer + 5, "%s", hari);
            showhari(hari, respon);
            printf("Received: %s %s\n\n", message, hari);
        }else if(strncmp(message, "add", 3) == 0){
            char add[100];
            sscanf(buffer + 4, "%[^\n]", add);
            adding(add, respon);
            printf("Received: %s %s\n\n", message, add);
        }else if(strncmp(message, "delete", 6) == 0){
            char delete[100];
            sscanf(buffer + 7, "%[^\n]", delete);
            deleteing(delete, respon);
            printf("Received: %s %s\n\n", message, delete);
        }else{
            strcpy(respon, "Invalid Command\n");
        }

        send(new_socket, respon, strlen(respon), 0);
        memset(respon, 0, sizeof(respon));
        memset(buffer, 0, sizeof(buffer));

    }

    close(new_socket);
    close(server_fd);
    
    return 0;
}