#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h> 
#include <openssl/evp.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static const char *dirpath = "/home/nicholas/modul4";

int isInsideDisableArea(const char *path)
{
    const char *disableAreaPath = "/disable-area";

    return strncmp(path, disableAreaPath, strlen(disableAreaPath)) == 0;
}

void reverseString(char *str, int start, int end)
{
    while (start < end)
    {
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;

 
        start++;
        end--;
    }
}

static void log_info(const char *tag, const char *information, int success) {
    time_t now;
    struct tm timestamp;
    char time_buffer[20];

    time(&now);
    localtime_r(&now, &timestamp);
    strftime(time_buffer, sizeof(time_buffer), "%d/%m/%Y-%H:%M:%S", &timestamp);

    FILE *log_file = fopen("/home/nicholas/modul4/logs-fuse.log", "a");
    if (log_file == NULL) {
        log_file = fopen("/home/nicholas/modul4/logs-fuse.log", "w");
    }

    if (log_file != NULL) {
        if (success) {
            fprintf(log_file, "[SUCCESS]::%s::%s::%s\n", time_buffer, tag, information);
        } else {
            fprintf(log_file, "[FAILED]::%s::%s::%s\n", time_buffer, tag, information);
        }
        fclose(log_file);
    }
}


void reverseString(char *str, int start, int end)
{
    while (start < end)
    {
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;

 
        start++;
        end--;
    }
}

void reverse_decode(char *input, size_t length) {
    reverseString(input, 0, length - 1);
}


int isInsideDisableArea(const char *path)
{
    const char *disableAreaPath = "/disable-area";

    return strncmp(path, disableAreaPath, strlen(disableAreaPath)) == 0;
}



int isInsideGallery(const char *path)
{
    const char *galleryPath = "/gallery";

    return strncmp(path, galleryPath, strlen(galleryPath)) == 0;
}


char* base64_decode(const char* input, size_t length) {
    BIO *b64, *bmem;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bmem = BIO_new_mem_buf((void*)input, length);
    bmem = BIO_push(b64, bmem);

    char* buffer = (char*)malloc(length);
    if (!buffer) {
        BIO_free_all(bmem);
        return NULL;
    }

    int decoded_size = BIO_read(bmem, buffer, length);
    if (decoded_size < 0) {
        free(buffer);
        BIO_free_all(bmem);
        return NULL;
    }

    buffer[decoded_size] = '\0';
    BIO_free_all(bmem);

    return buffer;
}
void rot13_decode(char* input, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        if ((input[i] >= 'A' && input[i] <= 'Z') || (input[i] >= 'a' && input[i] <= 'z')) {
            char base = (input[i] >= 'A' && input[i] <= 'Z') ? 'A' : 'a';
            input[i] = (input[i] - base + 13) % 26 + base;
        }
    }
}
char* hex_decode(const char* input, size_t length) {
    size_t i, j;
    char* output = (char*)malloc(length / 2 + 1);

    for (i = j = 0; i < length; i += 2) {
        sscanf(input + i, "%2hhx", output + j);
        j++;
    }

    output[j] = '\0';
    return output;
}

static const char *correctPassword = "berhasil";
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        log_info("getattr", "Failed to get attributes", 0);
       
        return -errno;
    }

    log_info("getattr", "Attributes retrieved successfully", 1);

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    if (strncmp(path, "/disable-area", 13) == 0)
    {
        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);

        // Check the entered password
        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("readdir", "Invalid password", 0);
            return -EACCES;
        }
        log_info("readdir", "Password is correct", 1); 
    }

    dp = opendir(fpath);

    if (dp == NULL) {
        log_info("readdir", "Failed to read directory", 0);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char fullpath[1500];  

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        snprintf(fullpath, sizeof(fullpath), "%s/%s", fpath, de->d_name);

        res = filler(buf, de->d_name, &st, 0);

        if (res != 0) {
            log_info("readdir", "Failed to fill directory", 0);
            break;
        }
    }

    closedir(dp);
    log_info("readdir", "Directory read successfully", 1);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;


    if (strstr(path, "/tulisan/") != NULL)
    {
        fd = open(fpath, O_RDONLY);

        if (fd == -1)
        {
            log_info("read", "Failed to open file", 0);
            return -errno;
        }

        const char *filename = strrchr(path, '/') + 1;
        if (strstr(filename, "base64") != NULL)
        {
            char *encoded_content;
            size_t encoded_size;

            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            encoded_content = base64_decode(buf, res);
            if (!encoded_content)
            {
                close(fd);
                log_info("read", "Failed to decode Base64", 0);
                return -ENOMEM;
            }
            encoded_size = strlen(encoded_content);

            size = (size < encoded_size - offset) ? size : (encoded_size - offset);

            memcpy(buf, encoded_content + offset, size);

            // Clean up
            free(encoded_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "rot13") != NULL)
        {
         
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            rot13_decode(buf, res);

        
            size = (size < res - offset) ? size : (res - offset);

       
            memcpy(buf, buf + offset, size);

            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "hex") != NULL)
        {
         
            char *hex_content;
            size_t hex_size;

            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            hex_content = hex_decode(buf, res);
            if (!hex_content)
            {
                close(fd);
                log_info("read", "Failed to decode hex", 0);
                return -ENOMEM;
            }
            hex_size = strlen(hex_content);

            size = (size < hex_size - offset) ? size : (hex_size - offset);

            memcpy(buf, hex_content + offset, size);

            // Clean up
            free(hex_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "rev") != NULL)
        {

            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            reverse_decode(buf, res);


            size = (size < res - offset) ? size : (res - offset);


            memcpy(buf, buf + offset, size);

            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
    }
    else
    {
        fd = open(fpath, O_RDONLY);

        if (fd == -1)
        {
            log_info("read", "Failed to open file", 0);
            return -errno;
        }
    }


    res = pread(fd, buf, size, offset);

    if (res == -1)
    {
        res = -errno;
        log_info("read", "Failed to read file content", 0);
    }

    close(fd);

    log_info("read", "Read file content successfully", 1);
    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_info("mkdir", "Failed to create directory", 0);
        return -errno;
    }

    log_info("mkdir", "Directory created successfully", 1);

    return 0;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    int fd;
    sprintf(fpath, "%s%s", dirpath, path);

    int res;
    (void) fi;

    res = creat(fpath, mode);

    if (res == -1)
    {
        log_info("create", "Failed to create file", 0);
        return -errno;
    }

    close(res);

    if (strncmp(path, "/sisop/test", 11) == 0)
    {
        fd = open(fpath, O_RDWR);

        if (fd == -1)
        {
            log_info("create", "Failed to open file", 0);
            return -errno;
        }

        char buffer[1000];
        ssize_t bytesRead = read(fd, buffer, sizeof(buffer));

        if (bytesRead > 0)
        {
    
            int start = 0;
            int end = bytesRead - 1;
            reverseString(buffer, start, end);

            lseek(fd, 0, SEEK_SET);
            write(fd, buffer, bytesRead);
        }

        close(fd);

        log_info("create", "File created and content reversed successfully", 1);
        return 0;
    }

    log_info("create", "File created successfully", 1);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
  
    if (strncmp(path, "/disable-area", 13) == 0)
    {

        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);


        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("open", "Invalid password", 0);
            return -EACCES;
        }
    }

    log_info("open", "File opened successfully", 1);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    int fd;
    int res;

    (void)fi;

    sprintf(fpath, "%s%s", dirpath, path);

    fd = open(fpath, O_WRONLY);

    if (fd == -1)
    {
        log_info("write", "Failed to open file", 0);
        return -errno;
    }


    if (strncmp(path, "/sisop/test", 11) == 0)
    {
        char *buffer = strdup(buf);
        int start = 0;
        int end = size - 1;
        reverseString(buffer, start, end);
        res = pwrite(fd, buffer, size, offset);
        free(buffer);
    }
    else
    {
        res = pwrite(fd, buf, size, offset);
    }

    if (res == -1)
    {
        res = -errno;
        log_info("write", "Failed to write file content", 0);
    }

    close(fd);

    log_info("write", "Write file content successfully", 1);
    return res;
}


static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000], to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    
    if (strncmp(to, "/gallery/rev", 12) == 0)
    {

        char *filename = strrchr(to_path, '/') + 1;
        char *extension = strrchr(filename, '.');

        if (extension != NULL)
        {
            int start = 0;
            int end = (int)(extension - filename) - 1;
            reverseString(filename, start, end);
        }
    }

 
    if (strncmp(to, "/gallery/delete", 15) == 0)
    {

        remove(from_path);
        log_info("rename", "File deleted successfully", 1);
        return 0;
    }

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        res = -errno;
        log_info("rename", "Failed to rename file", 0);
    }

    log_info("rename", "File renamed successfully", 1);
    return res;
}


static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = chmod(fpath, mode);

    if (res == -1)
    {
        res = -errno;
        log_info("chmod", "Failed to change file permissions", 0);
    }

    log_info("chmod", "File permissions changed successfully", 1);
    return res;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .write = xmp_write, 
    .chmod = xmp_chmod,
    .read = xmp_read,
    .open = xmp_open,  
    .create = xmp_create, 
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
