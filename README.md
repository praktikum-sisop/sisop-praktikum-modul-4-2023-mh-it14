# Laporan Resmi Sisop Modul 4
### IT14
| NRP | Nama |
| ------ | ------ |
| 5027221001 | Dwiyasa Nakula |
| 5027221042 |Nicholas Marco Weinandra |
- [Soal 1](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-MH-IT14/-/blob/main/README.md#nomor-1)
- [Soal 2](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-MH-IT14/-/blob/main/README.md#nomor-2)
- [Soal 3](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-MH-IT14/-/blob/main/README.md#nomor-3)

# Nomor 1
<details><summary>Click to expand</summary>
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.
Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:
Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
			Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
Output: eTgj80q7Ut7eNhNVooBE.HEIC
Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus.
Ex: "mv coba-deh.jpg delete-foto/" 
Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 
Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  
Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.
Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.
Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.
Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.
Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 
Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 
Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:
[SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6

</details>

## Pengerjaan Soal Nomor 1
[Source Code](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-mh-it14/-/tree/main/soal1)


```c
void reverseString(char *str, int start, int end)
{
    while (start < end)
    {
     
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;

        start++;
        end--;
    }
}
```
Fungsi `reverseString` berfungsi untuk membalik nama file.


```c
static void log_info(const char *tag, const char *information, int success) {
    time_t now;
    struct tm timestamp;
    char time_buffer[20];

    time(&now);
    localtime_r(&now, &timestamp);
    strftime(time_buffer, sizeof(time_buffer), "%d/%m/%Y-%H:%M:%S", &timestamp);

    FILE *log_file = fopen("/home/nicholas/modul4/logs-fuse.log", "a");
    if (log_file == NULL) {
        log_file = fopen("/home/nicholas/modul4/logs-fuse.log", "w");
    }

    if (log_file != NULL) {
        if (success) {
            fprintf(log_file, "[SUCCESS]::%s::%s::%s\n", time_buffer, tag, information);
        } else {
            fprintf(log_file, "[FAILED]::%s::%s::%s\n", time_buffer, tag, information);
        }
        fclose(log_file);
    }
}
```
Fungsi `log_info` merupakan fungsi untuk membuka file logs-fuse.log kemudian menulis hasil dari setiap eksekusi apakah success atau fail.

```c
char* base64_decode(const char* input, size_t length) {
    BIO *b64, *bmem;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bmem = BIO_new_mem_buf((void*)input, length);
    bmem = BIO_push(b64, bmem);

    char* buffer = (char*)malloc(length);
    if (!buffer) {
        BIO_free_all(bmem);
        return NULL;
    }

    int decoded_size = BIO_read(bmem, buffer, length);
    if (decoded_size < 0) {
        free(buffer);
        BIO_free_all(bmem);
        return NULL;
    }

    buffer[decoded_size] = '\0';
    BIO_free_all(bmem);

    return buffer;
}
```
Fungsi `base64_decode` berfungsi untuk men-decode file yang berisi teks yang di encode menggunakan base64.

```c
void rot13_decode(char* input, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        if ((input[i] >= 'A' && input[i] <= 'Z') || (input[i] >= 'a' && input[i] <= 'z')) {
            char base = (input[i] >= 'A' && input[i] <= 'Z') ? 'A' : 'a';
            input[i] = (input[i] - base + 13) % 26 + base;
        }
    }
}
```
Fungsi `rot13_decode` berfungsi untuk men-decode file yang berisi teks yang di encode menggunakan rot13.

```c
char* hex_decode(const char* input, size_t length) {
    size_t i, j;
    char* output = (char*)malloc(length / 2 + 1);

    for (i = j = 0; i < length; i += 2) {
        sscanf(input + i, "%2hhx", output + j);
        j++;
    }

    output[j] = '\0';
    return output;
}

```
Fungsi `rot13_decode` berfungsi untuk men-decode file yang berisi teks yang di encode menggunakan rot13.

```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        log_info("getattr", "Failed to get attributes", 0);
       
        return -errno;
    }

    log_info("getattr", "Attributes retrieved successfully", 1);

    return 0;
}

```
Fungsi `xmp_getattr` bertujuan untuk mendapatkan atribut dari sebuah file pada fuse filesystem.

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    if (strncmp(path, "/disable-area", 13) == 0)
    {
        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);

        
        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("readdir", "Invalid password", 0);
            return -EACCES;
        }
        log_info("readdir", "Password is correct", 1); 
    }

    dp = opendir(fpath);

    if (dp == NULL) {
        log_info("readdir", "Failed to read directory", 0);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char fullpath[1500];  // 

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        snprintf(fullpath, sizeof(fullpath), "%s/%s", fpath, de->d_name);

        res = filler(buf, de->d_name, &st, 0);

        if (res != 0) {
            log_info("readdir", "Failed to fill directory", 0);
            break;
        }
    }

    closedir(dp);
    log_info("readdir", "Directory read successfully", 1);

    return 0;
}
```
Fungsi `xmp_readdir` bertujuan untuk membaca sebuah direktori pada fuse filesystem.

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;


    if (strstr(path, "/tulisan/") != NULL)
    {
        fd = open(fpath, O_RDONLY);

        if (fd == -1)
        {
            log_info("read", "Failed to open file", 0);
            return -errno;
        }

        const char *filename = strrchr(path, '/') + 1;
        if (strstr(filename, "base64") != NULL)
        {
            char *encoded_content;
            size_t encoded_size;

            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            encoded_content = base64_decode(buf, res);
            if (!encoded_content)
            {
                close(fd);
                log_info("read", "Failed to decode Base64", 0);
                return -ENOMEM;
            }
            encoded_size = strlen(encoded_content);

            size = (size < encoded_size - offset) ? size : (encoded_size - offset);

         
            memcpy(buf, encoded_content + offset, size);

            // Clean up
            free(encoded_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
```
Pada fungsi `xmp_readdir`, direktori akan dibaca kemudian akan dicek apakah file berada pada folder **tulisan**. Lalu akan dicek apakah nama file tersebut mengandung string 'base64'. Apabila benar, maka program akan men-decode isi file tersebut menggunakan base64.

```c
 else if (strstr(filename, "rot13") != NULL)
        {
            
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }


            rot13_decode(buf, res);

       
            size = (size < res - offset) ? size : (res - offset);

      
            memcpy(buf, buf + offset, size);

            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
```
Apabila nama file mengandung string 'rot13', maka isi file akan di-decode menggunakan rot13.

```c
 else if (strstr(filename, "hex") != NULL)
        {
            char *hex_content;
            size_t hex_size;

            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

 
            hex_content = hex_decode(buf, res);
            if (!hex_content)
            {
                close(fd);
                log_info("read", "Failed to decode hex", 0);
                return -ENOMEM;
            }
            hex_size = strlen(hex_content);

 
            size = (size < hex_size - offset) ? size : (hex_size - offset);

     
            memcpy(buf, hex_content + offset, size);

      
            free(hex_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
```
Apabila nama file mengandung string 'hex', maka isi file akan di-decode menggunakan hex. 

```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_info("mkdir", "Failed to create directory", 0);
        return -errno;
    }

    log_info("mkdir", "Directory created successfully", 1);

    return 0;
}
```
Fungsi `xmp_mkdir` digunakan agar kita dapat membuat direktori baru pada fuse filesystem.

```c
static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = chmod(fpath, mode);

    if (res == -1)
    {
        res = -errno;
        log_info("chmod", "Failed to change file permissions", 0);
    }

    log_info("chmod", "File permissions changed successfully", 1);
    return res;
}
```
Fungsi `xmp_chmod` berfungsi untuk mengganti permission file.

```c
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    int fd;
    sprintf(fpath, "%s%s", dirpath, path);

    int res;
    (void) fi;

    res = creat(fpath, mode);

    if (res == -1)
    {
        log_info("create", "Failed to create file", 0);
        return -errno;
    }

    close(res);

    // Reverse the content of the file if it has the prefix "test"
    if (strncmp(path, "/sisop/test", 11) == 0)
    {
        fd = open(fpath, O_RDWR);

        if (fd == -1)
        {
            log_info("create", "Failed to open file", 0);
            return -errno;
        }

        char buffer[1000];
        ssize_t bytesRead = read(fd, buffer, sizeof(buffer));

        if (bytesRead > 0)
        {
            // Reverse the content
            int start = 0;
            int end = bytesRead - 1;
            reverseString(buffer, start, end);

            // Move to the beginning of the file and write the reversed content
            lseek(fd, 0, SEEK_SET);
            write(fd, buffer, bytesRead);
        }

        close(fd);

        log_info("create", "File created and content reversed successfully", 1);
        return 0;
    }

    log_info("create", "File created successfully", 1);
    return 0;
}
```
Fungsi `xmp_create` berfungsi untuk membuat file baru pada fuse filesystem. Kemudian program akan mengecek apakah nama file memiliki prefix 'test'. Jika iya, maka program akan membalik (reverse) isi dari file tersebut. Program juga akan mencatat hasil eksekusi pada file log.

```c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    if (strncmp(path, "/disable-area", 13) == 0)
    {

        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);

        // Check the entered password
        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("open", "Invalid password", 0);
            return -EACCES;
        }
    }

    log_info("open", "File opened successfully", 1);
    return 0;
}
```
Fungsi `xmp_open` digunakan untuk mengecek apakah terdapat file pada folder **disable-area**. Jika iya, maka program akan meminta password untuk mengakses folder dan file yang ada di **disable-area**.

```c
static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000], to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    
    if (strncmp(to, "/gallery/rev", 12) == 0)
    {
        
        char *filename = strrchr(to_path, '/') + 1;
        char *extension = strrchr(filename, '.');

        if (extension != NULL)
        {
            int start = 0;
            int end = (int)(extension - filename) - 1;
            reverseString(filename, start, end);
        }
    }

    if (strncmp(to, "/gallery/delete", 15) == 0)
    {
        remove(from_path);
        log_info("rename", "File deleted successfully", 1);
        return 0;
    }

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        res = -errno;
        log_info("rename", "Failed to rename file", 0);
    }

    log_info("rename", "File renamed successfully", 1);
    return res;
}
```
Fungsi `xmp_rename` berfungsi untuk mengecek apakah folder tujuan memiliki prefix 'rev'. Jika iya, maka program akan membalik nama file yang akan dipindahkan pada folder tersebut.<br>
Sedangkan apabila folder tujuan memiliki prefix 'delete', maka program akan menghapus nama file yang akan dipindahkan ke folder tersebut. 

```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .write = xmp_write, 
    .chmod = xmp_chmod,
    .read = xmp_read,
    .open = xmp_open,  
    .create = xmp_create, 
};
```
Terakhir, kita menyimpan seluruh fuse operation menggunakan struct.

###Revisi
Memperbaiki fitur untuk membalik (reverse) isi file yang memiliki nama file dengan prefix 'test' pada folder **sisop**.

### Output Nomor 1 :
#### Memindahkan file ke folder rev-test
![output 1a](images/rev-test.png)
#### Memindahkan file ke folder delete-foto
![output 1a](images/delete-foto.png)
#### Reverse File dengan Prefix 'test'
![output 1b](images/cat_test.png)
#### Decode base64
![output 1c](images/decode_base64.png)
#### Decode rot13
![output 1c](images/decode_rot13.png)
#### Decode hex
![output 1c](images/decode_hex.png)
#### Decode rev
![output 1c](images/decode_rev.png)
#### Pencatatan pada file logs-fuse.log
![output 1e](images/log.png)

# Nomor 2
<details><summary>Click to expand</summary>
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada [link ini](https://drive.google.com/file/d/1MFQ6ds9lpbo9I6-QlXjyFPYyMEZpB9Tq/view?usp=drive_link). Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

- Membuat file **open-password.c**
    - untuk membaca file **zip-pass.txt**
    - Melakukan proses dekripsi base64 terhadap file tersebut
    - Lalu unzip home.zip menggunakan password hasil dekripsi
- Membuat file semangat.c
    - Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
**[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information] <br>
Ex:
[SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop**
    - Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
    - Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
        + documents: .pdf dan .docx. 
        + images: .jpg, .png, dan .ico. 
        + website: .js, .html, dan .json. 
        + sisop: .c dan .sh. 
        + text: .txt. 
        + aI: .ipynb dan .csv. 
    - Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu
        + Password terdapat di file password.bin
    - Pada folder website
        - Membuat file csv pada folder website dengan format ini: file,title,body
        - Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

    - Tidak dapat menghapus file / folder yang mengandung prefix **“restricted”**
Pada folder documents
    - Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut

- Membuat file server.c
    - Pada folder ai, terdapat file webtoon.csv
    - Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client.
        + Menampilkan seluruh judul
        + Menampilkan berdasarkan genre
        + Menampilkan berdasarkan hari
        + Menambahkan ke dalam file webtoon.csv
        + Melakukan delete berdasarkan judul
        + Selain command yang diberikan akan menampilkan tulisan “Invalid Command”
- Manfaatkan client.c pada folder sisop sebagai client
</details>

## Pengerjaan Soal Nomor 2
[Source Code](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-mh-it14/-/tree/main/soal2)

#### open_password.c
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <unistd.h>
```
Header file yang berisi library
```
char *base64_decode(const char *input, int length) {
    BIO *bio, *b64;
    char *buffer = (char *)malloc(length);
    memset(buffer, 0, length);

    bio = BIO_new_mem_buf((void *)input, length);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    BIO_read(bio, buffer, length);
    BIO_free_all(bio);

    return buffer;
}
```
- Fungsi ini, base64_decode, mengambil string input berkode Base64 dan panjangnya sebagai parameter. Ia menggunakan abstraksi BIO (Basic Input/Output) OpenSSL untuk mendekode Base64. Memori dialokasikan untuk data yang didekode (buffer), dan buffer diinisialisasi dengan nol. Ini menyiapkan rantai BIO untuk decoding Base64 dan membaca input Base64 ke dalam buffer. Terakhir, ini membebaskan sumber daya BIO dan mengembalikan data yang didekodekan.
```
int main() {
    FILE *file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        perror("Error opening zip-pass.txt");
        return 1;
    }
    char password_base64[100];
    fgets(password_base64, sizeof(password_base64), file);
    fclose(file);
```
Fungsi `main` dimulai dengan membuka file "zip-pass.txt" untuk dibaca. Jika ada kesalahan saat membuka file, pesan kesalahan akan dicetak, dan program keluar dengan kode kembali 1. Kata sandi yang dikodekan Base64 dibaca dari file menggunakan fgets dan disimpan dalam variabel password_base64. File kemudian ditutup.
```
    char *password = base64_decode(password_base64, strlen(password_base64));
    printf("Decrypted Password: %s\n", password);
```
Kata sandi yang dikodekan Base64 didekodekan menggunakan fungsi base64_decode, dan hasilnya disimpan dalam variabel kata sandi.
Kata sandi yang dicetak ke cmd. 
```
    char unzip_command[100];
    snprintf(unzip_command, sizeof(unzip_command), "unzip -P %s home.zip", password);
    int status = system(unzip_command);
    if (status == 0) {
        printf("Unzip successful!\n");
    } else {
        printf("Unzip failed. Please check the password.\n");
    }
    return 0;
}
```
Perintah untuk mengekstrak file ("home.zip") menggunakan kata sandi yang didekripsi dibuat dan disimpan dalam variabel unzip_command.Perintah dijalankan menggunakan fungsi sistem, dan status hasil disimpan dalam variabel status. Jika operasi unzip berhasil (statusnya 0), pesan sukses akan dicetak; jika tidak, pesan kegagalan akan dicetak.

#### semangat.c
```
static const char *dirpath = "/home/erazora/Sisop-Modul-4/Soal-2";
```
Variabel dirpath set ke jalur direktori root.

```
void log_to_file(const char *tag, const char *information, int success) {
    // Implementation of logging to a file.
}
```
Fungsi ini mencatat informasi ke file bernama logs-fuse.log. Ini mencakup stempel waktu, tag, informasi yang dicatat, dan apakah operasi berhasil.
```
int password_bin(const char *inputpass) {
    // Function to check whether the input password matches the one stored in 'password.bin'.
}
```
Fungsi ini membaca kata sandi dari file bernama password.bin dan membandingkannya dengan kata sandi masukan yang diberikan. Ia mengembalikan 1 jika kata sandinya cocok, 0 jika tidak.
```

#### server.c
```
void tampilfilm(char *respon) {
    FILE *file = fopen("./aI/webtoon.csv", "r");
    if(file == NULL){
        strcpy(respon, "coba di cek lagi mungkin salah path\n");
        return;
    }
    char isi[200];
    int nomor = 1;
    while(fgets(isi, sizeof(isi), file) != NULL){
        char linenomor[300];
        sprintf(linenomor, "%d. %s", nomor, isi);
        strcat(respon, linenomor);
        nomor++;
    }
    fclose(file);
}
```
Fungsi tampilfilm membaca isi file "webtoon.csv" dan memformatnya dengan nomor baris. Hasilnya disimpan di parameter respon.
```
void showgenre(char *genre, char *respon) {
    // Similar structure to tampilfilm but filters based on genre.
}

void showhari(char *hari, char *respon) {
    // Similar structure to tampilfilm but filters based on hari (day).
}

void adding(char *film, char *respon) {
    // Adds a film entry to the "webtoon.csv" file.
}

void deleteing(char *judul, char *respon) {
    // Deletes a film entry from the "webtoon.csv" file.
}
```
Fungsi showgenre membaca konten "webtoon.csv" dan memfilter hasilnya berdasarkan genre yang ditentukan. Hasil yang diformat disimpan di parameter respons.

Fungsi showgenre membaca konten "webtoon.csv" dan memfilter hasilnya berdasarkan genre yang ditentukan. Hasil yang diformat disimpan di parameter respons.

Fungsi adding menambahkan entri film baru ke file "webtoon.csv" berdasarkan informasi film yang diberikan. Hasilnya disimpan di parameter respon.

Fungsi deleting menghapus entri film dari file "webtoon.csv" berdasarkan judul (judul) yang disediakan. Hasilnya disimpan di parameter respon.

```
int main(int argc, char const *argv[]) {
       int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[2048] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
```
Kode ini mendeklarasikan variabel untuk operasi terkait soket, termasuk pembuatan soket (server_fd). Jika pembuatan soket gagal, pesan kesalahan akan dicetak, dan program keluar.
```
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
```
Ini menetapkan opsi soket untuk memungkinkan penggunaan kembali alamat dan port. Jika pengaturan opsi gagal, pesan kesalahan akan dicetak, dan program keluar.
```
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
```
Ini mengatur struktur alamat untuk soket dan mengikatnya ke port yang ditentukan. Jika pengikatan gagal, pesan kesalahan akan dicetak, dan program keluar.
```
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
```
Program ini mendengarkan koneksi masuk. Jika mendengarkan gagal, pesan kesalahan akan dicetak, dan program keluar.
```
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
```
Program ini menerima koneksi dari klien. Jika penerimaan koneksi gagal, pesan kesalahan akan dicetak, dan program keluar.
```
    while(1){
        char message[200];
        valread = read(new_socket, buffer, 2048);

        if(valread <= 0) break; //Kondisi ini memeriksa apakah operasi pembacaan berhasil. Jika tidak, maka akan keluar dari lingkaran dan mengakhiri komunikasi.

        sscanf(buffer, "%s", message); //Ini mengekstrak kata pertama dari pesan yang diterima dan menyimpannya dalam variabel pesan. Ini digunakan untuk menentukan jenis perintah.
        
        char respon[200];
        // Command handling berdasarkan pesan yang diterima.
         // Respon disimpan dalam variabel respon dan dikirim kembali ke klien.
    }
```
loop while(1) berlanjut selama masih ada pesan yang dibaca dari klien. Ia membaca pesan dari soket klien menggunakan fungsi baca ke dalam variabel buffer. Jika valread kurang dari atau sama dengan 0, maka valread akan keluar dari loop.
```
            if(strncmp(message, "tampilkan", 9) == 0){
                tampilfilm(respon);
                printf("Received: %s\n\n", message);
            }else if(strncmp(message, "genre", 5) == 0){
                char genre[100];
                sscanf(buffer + 6, "%s", genre);
                showgenre(genre, respon);
                printf("Received: %s %s\n\n", message, genre);
            }else if(strncmp(message, "hari", 4) == 0){
                char hari[100];
                sscanf(buffer + 5, "%s", hari);
                showhari(hari, respon);
                printf("Received: %s %s\n\n", message, hari);
            }else if(strncmp(message, "add", 3) == 0){
                char add[100];
                sscanf(buffer + 4, "%[^\n]", add);
                adding(add, respon);
                printf("Received: %s %s\n\n", message, add);
            }else if(strncmp(message, "delete", 6) == 0){
                char delete[100];
                sscanf(buffer + 7, "%[^\n]", delete);
                deleteing(delete, respon);
                printf("Received: %s %s\n\n", message, delete);
            }else{
                strcpy(respon, "Invalid Command\n");
            }
```
Bagian kode ini memeriksa jenis perintah berdasarkan pesan yang diterima.
- Jika perintahnya "tampilkan", maka fungsi tampilfilm akan dipanggil untuk menampilkan semua film.
- Jika perintahnya adalah "genre", perintah tersebut akan mengekstrak genre dari pesan dan memanggil fungsi showgenre untuk menampilkan film dengan genre tersebut.
- Jika perintahnya adalah "hari", ia akan mengekstrak hari dari pesan dan memanggil fungsi showhari untuk menampilkan film pada hari itu.
- Jika perintahnya adalah "tambah", ia mengekstrak informasi film dari pesan dan memanggil fungsi penambahan untuk menambahkan film baru.
- Jika perintahnya adalah "hapus", judul film akan diekstraksi dari pesan dan memanggil fungsi penghapusan untuk menghapus film tersebut.
- Jika tidak ada perintah yang dikenali yang diterima, respons "Perintah Tidak Valid" akan ditetapkan.
```
            send(new_socket, respon, strlen(respon), 0);
            memset(respon, 0, sizeof(respon));
            memset(buffer, 0, sizeof(buffer));
        }
```
Respons yang disimpan dalam respons dikirim kembali ke klien menggunakan fungsi kirim. Buffer respons dan buffer pesan yang diterima kemudian dihapus untuk iterasi berikutnya.

### Output Nomor 2 :
#### Output `open-password.c`
![output 2a](images/open_password.png)
#### Output `semangat.c`
![output 2b](images/semangat.png)
#### Output `server.c`
![output 2c](images/server.png)

### Revisi
Terdapat revisi pada soal ini, mengenai folder website, restricted prefix, documents, dan sudah selesai di revisi. pada modul ini terdapat kesusahan dalam mempelajari materi FUSE, yang mana sangat berbeda dari Modul-modul yang lain.

# Nomor 3
<details><summary>Click to expand</summary>
Dalam kegelapan malam yang sepi, bulan purnama menerangi langit dengan cahaya peraknya. Rumah-rumah di sekitar daerah itu terlihat seperti bayangan yang menyelimuti jalan-jalan kecil yang sepi. Suasana ini terasa aneh, membuat hati seorang wanita bernama Sarah merasa tidak nyaman. <br>
Dia berjalan ke jendela kamarnya, menatap keluar ke halaman belakang. Pohon-pohon besar di halaman tampak gelap dan menyeramkan dalam sinar bulan. Sesekali, cahaya bulan yang redup itu memperlihatkan bayangan-bayangan aneh yang bergerak di antara pepohonan.<br>
Tiba-tiba, Ting! Pandangannya tertuju pada layar smartphonenya. Dia terkejut bukan kepalang. Notifikasi barusan adalah remainder pengumpulan praktikum yang dikirim oleh asisten praktikumnya. Sarah lupa bahwa 2 jam lagi adalah waktu pengumpulan tugas praktikum mata kuliah Sistem Operasi. Sarah sejenak merasa putus asa, saat dia menyadari bahwa ketidaknyamanan yang dirasakannya adalah akibat tekanan tugas praktikumnya.<br>
Sarah melihat tugas praktikum yang harus dia selesaikan, dan dalam hitungan detik, rasa panik melanda. Tugas ini tampaknya sangat kompleks, dan dia belum sepenuhnya siap. Sebagai seorang mahasiswa teknik komputer, mata kuliah Sistem Operasi adalah salah satu mata kuliah yang sangat menuntut, dan dia harus menyelesaikan tugas ini dengan baik untuk menjaga nilai akademiknya. <br>
Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:

- Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
- Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.
- Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
    - Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
    - Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
    - Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Contoh:
REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
FLAG::231105-12:29:33::RMDIR::/home/sarah/folder

- Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

Contoh :
/tmp/test_fuse/ adalah filesystem yang harus dirancang.
/home/index/modular/ adalah direktori asli.
</details>

## Pengerjaan Soal Nomor 3
[Source Code](https://gitlab.com/praktikum-sisop/sisop-praktikum-modul-4-2023-mh-it14/-/tree/main/soal3)

Pada soal nomor 3, praktikan diminta untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:

- Direktori asal (/home/user/modular) terhubung dengan direktori tujuan (/tmp/test_fuse)
- Perintah touch, mkdir, rmdir, dan rm/rm -rf disimpan ke dalam fs_module.log
- Ketika dilakukan modularisasi, file pada direktori asal (di dalam folder dengan nama awalan module_) akan dipecah menjadi file-file kecil sebesar 1024 byte

- Pada direktori tujuan, file tidak akan dipecah dan tetap utuh seperti semula
- Apabila folder dengan nama awalan module_ diubah, maka file pada direktori asal akan menjadi utuh

```
static const char *dirpath = "/home/erazora/modular";
```
Path dasar dari filesystem yang akan dibuat.

```
void log_command(const char *level, const char *cmd, const char *desc, ...) {
    time_t t;
    struct tm *tm_info;
    time(&t);
    tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/erazora/fs_module.log", "a");
    if (log_file != NULL)
    {
        va_list args;
        va_start(args, desc);

        fprintf(log_file, "[%s]::%s::%s::", level, timestamp, cmd);
        vfprintf(log_file, desc, args);
        fprintf(log_file, "\n");

        va_end(args);
        fclose(log_file);
    }
}
```
membuat log ke fs_module.log

```
void modularize_file(const char *path) {
    FILE *input_file = fopen(path, "rb");
    if (input_file == NULL)
    {
        return;
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    rewind(input_file);

    if (file_size <= 1024)
    {
        fclose(input_file);
        return;
    }

    int count = (file_size + 1023) / 1024;

    for (int i = 1; i <= count; i++)
    {
        char chunk_path[256];
        snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", path, i);
        FILE *output_file = fopen(chunk_path, "wb");

        if (output_file == NULL)
        {
            fclose(input_file);
            return;
        }

        char *buffer = (char *)malloc(1024);
        if (buffer == NULL)
        {
            fclose(input_file);
            fclose(output_file);
            return;
        }

        size_t bytes = fread(buffer, 1, 1024, input_file);
        fwrite(buffer, 1, bytes, output_file);

        free(buffer);
        fclose(output_file);
    }

    fclose(input_file);
}
```
Fungsi untuk membagi file menjadi chunk seukuran 1024 byte dan menyimpannya dengan nama yang berbeda.

```
static int xmp_utimens(const char *path, const struct timespec tv[2])
```
Melibatkan pengaturan waktu (timestamps) pada file.

```
static int xmp_unlink(const char *path)
static int xmp_rmdir(const char *path)
static int xmp_rename(const char *from, const char *to)
static int xmp_mkdir(const char *path, mode_t mode)
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
static int xmp_getattr(const char *path, struct stat *stbuf)
void deleteFiles(const char *path)
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
```
xmp_create, xmp_write, xmp_unlink, Melibatkan pembuatan, penulisan, dan penghapusan file, serta logging operasi tersebut.
Operasi-operasi Filesystem: Operasi-operasi filesystem yang dilakukan pada xmp_readdir, seperti modularisasi file jika di dalam direktori tertentu.

xmp_unlink(konstan karakter *jalur)
Fungsi ini dipanggil ketika sistem file ingin menghapus file.
Ini menghapus file yang ditentukan oleh jalur yang diberikan.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_rmdir(konstan karakter *jalur)
Fungsi ini dipanggil ketika sistem file ingin menghapus direktori.
Ini menghapus direktori yang ditentukan oleh jalur yang diberikan.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_rename(const char *dari, const char *ke)
Fungsi ini dipanggil ketika sistem file ingin mengganti nama file atau direktori.
Ini mengganti nama file atau direktori dari dari ke menjadi.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_mkdir(const char *jalur, mode_t mode)
Fungsi ini dipanggil ketika sistem file ingin membuat direktori baru.
Itu membuat direktori baru dengan jalur dan mode yang ditentukan.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_write(const char *path, const char *buf, ukuran size_t, offset off_t, struct fuse_file_info *fi)
Fungsi ini dipanggil ketika sistem file ingin menulis data ke file.
Itu menulis data dalam buffer (buf) ke file yang ditentukan oleh jalur.
Parameter ukuran menunjukkan ukuran data, dan offset adalah offset dalam file.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_create(const char *jalur, mode mode_t, struct fuse_file_info *fi)
Fungsi ini dipanggil ketika sistem file ingin membuat file baru.
Itu membuat file baru dengan jalur dan mode yang ditentukan.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_read(const char *path, char *buf, ukuran size_t, offset off_t, struct fuse_file_info *fi)
Fungsi ini dipanggil ketika sistem file ingin membaca data dari suatu file.
Ia membaca data dari file yang ditentukan oleh jalur ke buffer (buf).
Parameter size menunjukkan ukuran data yang akan dibaca, dan offset adalah offset dalam file.
Ini mencatat operasi menggunakan fungsi log_command.

xmp_getattr(const char *jalur, struct stat *stbuf)
Fungsi ini dipanggil ketika sistem file ingin mendapatkan atribut file (misalnya ukuran, izin).
Ini mengambil atribut file yang ditentukan oleh jalur dan menyimpannya dalam struktur stbuf.

deleteFiles(const char *jalur)
Fungsi ini digunakan untuk menghapus file dalam direktori yang sesuai dengan pola tertentu (file dengan nama berakhiran tiga digit).
Ini mengulangi file di direktori (jalur) yang ditentukan dan menghapus file yang cocok dengan polanya.
Ini mencatat penghapusan setiap file.


```
static struct fuse_operations xmp_oper = { 
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .create = xmp_create,
    .write = xmp_write,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
};
```
Mendefinisikan operasi-operasi FUSE yang akan digunakan.


### Output Nomor 3:
#### Output `easy.c`
![output 3a](images/module_.png)
![output 3b](images/modular.png)
![output 3c](images/fs_module.png)

### Revisi
Tidak ada revisi pada soal ini
