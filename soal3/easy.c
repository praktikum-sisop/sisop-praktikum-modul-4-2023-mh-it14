#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <regex.h>

static const char *dirpath = "/home/erazora/modular";

void log_command(const char *level, const char *cmd, const char *desc, ...)
{
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/erazora/fs_module.log", "a");
    if (log_file != NULL)
    {
        va_list args;
        va_start(args, desc);

        fprintf(log_file, "[%s]::%s::%s::", level, timestamp, cmd);
        vfprintf(log_file, desc, args);
        fprintf(log_file, "\n");

        va_end(args);

        fclose(log_file);
    }
}

void modularize_file(const char *path)
{
    FILE *input_file = fopen(path, "rb");
    if (input_file == NULL)
    {
        return;
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    rewind(input_file);

    if (file_size <= 1024)
    {
        fclose(input_file);
        return;
    }

    int count = (file_size + 1023) / 1024;

    for (int i = 1; i <= count; i++)
    {
        char chunk_path[256];
        snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", path, i);
        FILE *output_file = fopen(chunk_path, "wb");

        if (output_file == NULL)
        {
            fclose(input_file);
            return;
        }

        char *buffer = (char *)malloc(1024);
        if (buffer == NULL)
        {
            fclose(input_file);
            fclose(output_file);
            return;
        }

        size_t bytes = fread(buffer, 1, 1024, input_file);
        fwrite(buffer, 1, bytes, output_file);

        free(buffer);
        fclose(output_file);
    }

    fclose(input_file);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

int dot_3_digit(const char *filename)
{
    const char *pattern = ".*\\.[0-9]{3}$";
    regex_t regex;

    if (regcomp(&regex, pattern, REG_EXTENDED))
        return 0;

    int match = !regexec(&regex, filename, 0, NULL, 0);
    regfree(&regex);

    return match;
}

void deleteFiles(const char *path)
{
    DIR *dir = opendir(path);

    if (dir == NULL)
    {
        perror("Error opening directory");
        return;
    }

    struct dirent *entry;

    while ((entry = readdir(dir)) != NULL)
    {
        if (dot_3_digit(entry->d_name))
        {
            char full_path[1000];
            sprintf(full_path, "%s/%s", path, entry->d_name);

            if (remove(full_path) != 0)
            {
                perror("Error deleting file");
            }
            else
            {
                printf("Deleted file: %s\n", full_path);
            }
        }
    }

    closedir(dir);
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
    {
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char full_path[1256];
        sprintf(full_path, "%s/%s", fpath, de->d_name);

        if (strstr(fpath, "/module_"))
        {
            if (de->d_type == DT_REG && !dot_3_digit(de->d_name))
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                modularize_file(full_path);
            }
        }
        else
        {
            if (de->d_type == DT_DIR || de->d_type == DT_REG)
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                deleteFiles(fpath);
            }
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
    {
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
    {
        res = -errno;
    }

    close(fd);

    return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags, mode);

    if (res == -1)
    {
        log_command("REPORT", "CREATE", "Error creating file %s", fpath);
        return -errno;
    }

    fi->fh = res;

    log_command("REPORT", "CREATE", fpath);

    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = fi->fh;
    int res = pwrite(fd, buf, size, offset);

    if (res == -1)
    {
        log_command("REPORT", "WRITE", "Error writing to file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "WRITE", "Write to file %s", fpath);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_command("REPORT", "MKDIR", "Error creating directory %s", fpath);
        return -errno;
    }

    log_command("REPORT", "MKDIR", "Directory created: %s", fpath);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000];
    char to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        log_command("REPORT", "RENAME", "Error renaming %s to %s", from_path, to_path);
        return -errno;
    }

    log_command("REPORT", "RENAME", "%s to %s", from_path, to_path);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = rmdir(fpath);

    if (res == -1)
    {
        log_command("FLAG", "RMDIR", "Error removing directory %s", fpath);
        return -errno;
    }

    log_command("FLAG", "RMDIR", fpath);

    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = unlink(fpath);

    if (res == -1)
    {
        log_command("REPORT", "UNLINK", "Error unlinking file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "UNLINK", fpath);

    return 0;
}

static int xmp_utimens(const char *path, const struct timespec tv[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = utimensat(0, fpath, tv, AT_SYMLINK_NOFOLLOW);
    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .create = xmp_create,
    .write = xmp_write,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}